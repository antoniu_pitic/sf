﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SF
{
    public partial class Principala : Form
    {
        public Principala()
        {
            InitializeComponent();
        }

        private void SFButton_Click(object sender, EventArgs e)
        {
            Grile g = new Grile();
            DialogResult rez = g.ShowDialog();

            switch (rez)
            {
                case DialogResult.None:
                    Console.WriteLine("ai inchis fara sa alegi");
                    break;
                case DialogResult.OK:
                    Console.WriteLine("");
                    break;
                case DialogResult.Cancel:
                    Console.WriteLine("nu vrea sa raaspunda");
                    break;
                case DialogResult.Abort:
                    break;
                case DialogResult.Retry:
                    break;
                case DialogResult.Ignore:
                    break;
                case DialogResult.Yes:
                    Console.WriteLine("Da");
                    break;
                case DialogResult.No:
                    Console.WriteLine("NU");
                    break;
                default:
                    break;
            }
        }
    }
}
