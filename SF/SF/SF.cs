﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SF
{
    public partial class SF : Form
    {

      
        public SF()
        {
            InitializeComponent();
        }

        private void loadButton_Click(object sender, EventArgs e)
        {
            if (openTextFileDialog.ShowDialog() == DialogResult.OK)
            {
                inputTextBox.Text = File.ReadAllText(openTextFileDialog.FileName);

            }
        }

        private void encodeButton_Click(object sender, EventArgs e)
        {
            
            ShannonFano sf = new ShannonFano();
            sf.Encode(inputTextBox.Text);
        }

        private void inputTextBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
