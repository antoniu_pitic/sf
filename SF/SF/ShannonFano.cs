﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SF
{

    internal class ShannonFano
    {
        string input;
        List<(char c, int ct, string cod)> statistica = new List<(char, int, string)>();
        string output;
        internal void Encode(string deCodat)
        {
            input = deCodat;
            GenerateStatistics();
        }

        private void GenerateStatistics()
        {
            statistica.Clear();

            for (int i = 0; i <= 255; i++)
            {
                statistica.Add(((char)(i), 0, ""));
            }

            foreach (char c in input)
            {
                statistica[c] = (statistica[c].c, statistica[c].ct + 1, "");
            }

            statistica.RemoveAll(x => (x.ct == 0));

            statistica.Sort((x, y) => x.ct - y.ct);

            GenerateCodes(0, statistica.Count - 1);

            GenerateOutput();

            DebugInfo();
        }

        private void GenerateOutput()
        {
            output = "";

            foreach (char c in input)
            {
                output += (statistica.Find(x => x.c == c).cod);
            }
        }

        private void GenerateCodes(int st, int dr)
        {
            int m = Mijloc(st, dr);

            for (int i = st; i <= m; i++)
            {
                statistica[i] = (statistica[i].c, statistica[i].ct, statistica[i].cod + "0");
            }
            for (int i = m + 1; i <= dr; i++)
            {
                statistica[i] = (statistica[i].c, statistica[i].ct, statistica[i].cod + "1");
            }
            if (st < dr)
            {
                GenerateCodes(st, m);
                GenerateCodes(m + 1, dr);
            }
        }

        private int Mijloc(int st, int dr)
        {
            int Sdr = 0, Sst = 0;
            int i = st, j = dr;
            while (i <= j)
            {
                if (Sst < Sdr)
                {
                    Sst += statistica[i].ct;
                    i++;
                }
                else
                {
                    Sdr += statistica[j].ct;
                    j--;
                }
            }
            return j;
        }

        private void DebugInfo()
        {
            foreach (var t in statistica)
            {
                Console.WriteLine(t.c + "-" + t.ct + "-" + t.cod);
            }

            Console.WriteLine(output);
        }
    }


}