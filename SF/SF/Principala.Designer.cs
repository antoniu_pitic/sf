﻿namespace SF
{
    partial class Principala
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrilaButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // GrilaButton
            // 
            this.GrilaButton.Location = new System.Drawing.Point(52, 55);
            this.GrilaButton.Name = "GrilaButton";
            this.GrilaButton.Size = new System.Drawing.Size(194, 95);
            this.GrilaButton.TabIndex = 0;
            this.GrilaButton.Text = "Grila";
            this.GrilaButton.UseVisualStyleBackColor = true;
            this.GrilaButton.Click += new System.EventHandler(this.SFButton_Click);
            // 
            // Principala
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 630);
            this.Controls.Add(this.GrilaButton);
            this.Name = "Principala";
            this.Text = "Principala";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GrilaButton;
    }
}